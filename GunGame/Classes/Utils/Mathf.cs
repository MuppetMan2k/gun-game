﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GunGame
{
    static class Mathf
    {
        /* -------- Properties -------- */
        public static float Pi => 3.14159265359f;
        public static float TwoPi => 6.28318530718f;

        /* -------- Public Methods -------- */
        public static int Max(params int[] vals)
        {
            int max = int.MinValue;
            foreach (int val in vals)
                if (val > max)
                    max = val;
            return max;
        }
        public static int Min(params int[] vals)
        {
            int min = int.MaxValue;
            foreach (int val in vals)
                if (val < min)
                    min = val;
            return min;
        }

        public static float ToDegrees(float radians) => radians * 180f / Pi;
        public static float ToRadians(float degrees) => degrees * Pi / 180f;
    }
}
