﻿using static GunGame.Mathf;

namespace GunGame
{
    struct AngleDifference
    {
        /* -------- Properties -------- */
        public float Radians => radians;
        public float Degrees => ToDegrees(radians);

        /* -------- Private Fields -------- */
        private float radians;

        /* -------- Constructors -------- */
        public AngleDifference(float rads)
        {
            radians = rads;
            Normalize();
        }

        /* -------- Operator Overloads -------- */
        public static AngleDifference operator +(AngleDifference diff1, AngleDifference diff2) => FromRadians(diff1.Radians + diff2.Radians);
        public static AngleDifference operator -(AngleDifference diff1, AngleDifference diff2) => FromRadians(diff1.Radians - diff2.Radians);

        /* -------- Private Methods -------- */
        private void Normalize()
        {
            while (radians <= -Pi)
                radians += TwoPi;
            while (radians > Pi)
                radians -= TwoPi;
        }

        /* -------- Static Methods -------- */
        public static AngleDifference FromDegrees(float degs) => new AngleDifference(ToRadians(degs));
        public static AngleDifference FromRadians(float rads) => new AngleDifference(rads);
    }
}
