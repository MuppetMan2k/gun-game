﻿using static GunGame.Mathf;

namespace GunGame
{
    struct Angle
    {
        /* -------- Properties -------- */
        public float Radians => radians;
        public float Degrees => ToDegrees(radians);

        /* -------- Private Fields -------- */
        private float radians;

        /* -------- Constructors -------- */
        public Angle(float rads)
        {
            radians = rads;
            Normalize();
        }

        /* -------- Operator Overloads -------- */
        public static Angle operator +(Angle angle, AngleDifference diff) => FromRadians(angle.Radians + diff.Radians);
        public static Angle operator -(Angle angle, AngleDifference diff) => FromRadians(angle.Radians - diff.Radians);

        /* -------- Private Methods -------- */
        private void Normalize()
        {
            while (radians < 0f)
                radians += TwoPi;
            while (radians >= TwoPi)
                radians -= TwoPi;
        }

        /* -------- Static Methods -------- */
        public static Angle FromDegrees(float degs) => new Angle(ToRadians(degs));
        public static Angle FromRadians(float rads) => new Angle(rads);
    }
}
