﻿using Microsoft.Xna.Framework;

namespace GunGame
{
    static class SpaceUtils
    {
        /* -------- Public Methods -------- */
        public static Vector2 ConvertPSToSS(Point ps, GraphicsComponent g)
        {
            Vector2 ss = Vector2.Zero;
            ss.X = (float)(ps.X - g.HalfScreenWidth) / g.HalfScreenWidth;
            ss.Y = (float)(g.HalfScreenHeight - ps.Y) / g.HalfScreenHeight;
            return ss;
        }
    }
}
