﻿using System;

namespace GunGame
{
    static class EnumUtils
    {
        /* -------- Public Methods -------- */
        public static T[] GetEnumStates<T>()
        {
            T[] array = (T[])Enum.GetValues(typeof(T));
            return array;
        }
    }
}
