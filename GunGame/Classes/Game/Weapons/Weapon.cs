﻿namespace GunGame
{
    class Weapon
    {
        /* -------- Properties -------- */
        public string Name { get; private set; }

        /* -------- Constructors -------- */
        public Weapon(string inName)
        {
            Name = inName;
        }
    }
}
