﻿using Microsoft.Xna.Framework;

namespace GunGame
{
    struct GameObjectPosition
    {
        /* -------- Public Fields -------- */
        public Vector2 Position;
        public Angle Angle;

        /* -------- Constructors -------- */
        public GameObjectPosition(Vector2 inPosition, Angle inAngle)
        {
            Position = inPosition;
            Angle = inAngle;
        }
    }
}
