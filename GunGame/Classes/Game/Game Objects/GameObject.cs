﻿namespace GunGame
{
    class GameObject
    {
        /* -------- Properties -------- */
        public GameObjectPosition Position { get; protected set; }
        public GameObjectHealth Health { get; protected set; }
        public bool IsAlive => Health.Health > 0;
    }
}
