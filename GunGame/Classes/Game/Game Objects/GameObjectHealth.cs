﻿namespace GunGame
{
    struct GameObjectHealth
    {
        /* -------- Public Fields -------- */
        public int MaxHealth;
        public int Health;

        /* -------- Constructors -------- */
        public GameObjectHealth(int inMaxHealth, int inHealth)
        {
            MaxHealth = inMaxHealth;
            Health = inHealth;
        }
    }
}
