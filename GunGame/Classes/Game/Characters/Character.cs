﻿using System;
using System.Collections.Generic;

namespace GunGame.Classes
{
    class Character : GameObject
    {
        /* -------- Properties -------- */
        public Weapon CurrentWeapon { get; private set; }
        public List<Weapon> Weapons { get; private set; }
    }
}
