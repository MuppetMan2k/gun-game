﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace GunGame
{
    class GameTimeWrapper
    {
        /* -------- Properties -------- */
        public float ElapsedSeconds { get; private set; }

        /* -------- Constructors -------- */
        public GameTimeWrapper(GameTime gt)
        {
            ElapsedSeconds = (float)gt.ElapsedGameTime.TotalSeconds;
        }
    }
}
