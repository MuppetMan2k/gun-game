﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GunGame
{
    class GraphicsComponent
    {
        /* -------- Properties -------- */
        public GraphicsDeviceManager GraphicsDeviceManager { get; private set; }
        public GraphicsDevice GraphicsDevice { get; private set; }
        public SpriteBatch SpriteBatch { get; private set; }
        public int ScreenWidth { get; private set; }
        public int ScreenHeight { get; private set; }
        public int HalfScreenWidth => ScreenWidth / 2;
        public int HalfScreenHeight => ScreenHeight / 2;
        public float AspectRatio => (float)ScreenWidth / ScreenHeight;

        /* -------- Constructors -------- */
        public GraphicsComponent(Game inGame)
        {
            GraphicsDeviceManager = new GraphicsDeviceManager(inGame);

            // DEBUG
            SetResolution(800, 600, false);
        }

        /* -------- Public Methods -------- */
        public void SetGraphicsDevice(GraphicsDevice inGraphicsDevice)
        {
            GraphicsDevice = inGraphicsDevice;
        }
        public void CreateSpriteBatch()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
        }

        public void SetResolution(int width, int height, bool fullScreen)
        {
            ScreenWidth = width;
            ScreenHeight = height;

            GraphicsDeviceManager.PreferredBackBufferWidth = width;
            GraphicsDeviceManager.PreferredBackBufferHeight = height;
            GraphicsDeviceManager.IsFullScreen = fullScreen;
            GraphicsDeviceManager.ApplyChanges();
        }

        public void Clear(Color color)
        {
            GraphicsDevice.Clear(color);
        }
    }
}
