﻿namespace GunGame
{
    /* -------- Enumerations -------- */
    enum eTargetFlowState
    {
        MENU,
        GAME
    }

    class FlowStateChangeRequest
    {
        /* -------- Properties -------- */
        public eTargetFlowState TargetFlowState { get; private set; }

        /* -------- Constructors -------- */
        public FlowStateChangeRequest(eTargetFlowState inTargetFlowState)
        {
            TargetFlowState = inTargetFlowState;
        }
    }
}
