﻿using System;

namespace GunGame
{
    class SplashFlowState : FlowState
    {
        /* -------- Constructors -------- */
        public SplashFlowState(FlowComponent inFlowComponent, IServiceProvider inServiceProvider)
            : base(inFlowComponent, inServiceProvider)
        {
        }

        /* -------- Public Methods -------- */
        public override void Update(GameTimeWrapper gtw)
        {
#if DEBUG
            flowComponent.SubmitFlowStateChangeRequest(new FlowStateChangeRequest(eTargetFlowState.MENU));
#endif
        }

        public override void Draw(GraphicsComponent g)
        {
            
        }
    }
}
