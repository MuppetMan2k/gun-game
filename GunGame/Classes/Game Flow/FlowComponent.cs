﻿using System;

namespace GunGame
{
    class FlowComponent
    {
        /* -------- Private Fields -------- */
        private FlowState flowState;
        private FlowStateChangeRequest changeRequest;
        private IServiceProvider serviceProvider;
        private LoadingScreen loadingScreen;

        /* -------- Constructors -------- */
        public FlowComponent(IServiceProvider inServiceProvider)
        {
            serviceProvider = inServiceProvider;

            loadingScreen = new LoadingScreen();
            flowState = new SplashFlowState(this, serviceProvider);
        }

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw)
        {
            if (changeRequest != null)
                PerformFlowStateChange();

            if (loadingScreen.HasLoadingToProcess)
                loadingScreen.Update();
            else
                flowState.Update(gtw);
        }

        public void Draw(GraphicsComponent g)
        {
            if (loadingScreen.HasLoadingToProcess)
                loadingScreen.Draw(g);
            else
                flowState.Draw(g);
        }

        public void SubmitFlowStateChangeRequest(FlowStateChangeRequest inChangeRequest)
        {
            changeRequest = inChangeRequest;
        }

        /* -------- Private Methods -------- */
        private void PerformFlowStateChange()
        {
            flowState.UnloadContent();

            switch (changeRequest.TargetFlowState)
            {
                case eTargetFlowState.GAME:
                    flowState = new GameFlowState(loadingScreen, this, serviceProvider);
                    break;
                case eTargetFlowState.MENU:
                    flowState = new MenuFlowState(this, serviceProvider);
                    break;
            }

            changeRequest = null;
        }
    }
}
