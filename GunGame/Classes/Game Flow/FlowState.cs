﻿using System;
using Microsoft.Xna.Framework.Content;

namespace GunGame
{
    abstract class FlowState
    {
        /* -------- Private Fields -------- */
        protected FlowComponent flowComponent;
        protected ContentManager contentManager;

        /* -------- Constructors -------- */
        public FlowState(FlowComponent inFlowComponent, IServiceProvider inServiceProvider)
        {
            flowComponent = inFlowComponent;
            contentManager = new ContentManager(inServiceProvider, "Content");
        }

        /* -------- Public Methods -------- */
        public abstract void Update(GameTimeWrapper gtw);
        public abstract void Draw(GraphicsComponent g);
        public virtual void UnloadContent()
        {
            contentManager.Unload();
            contentManager.Dispose();
        }
    }
}
