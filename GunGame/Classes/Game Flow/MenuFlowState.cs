﻿using System;
using Microsoft.Xna.Framework;

namespace GunGame
{
    class MenuFlowState : FlowState
    {
        /* -------- Constructors -------- */
        public MenuFlowState(FlowComponent inFlowComponent, IServiceProvider inServiceProvider)
            : base(inFlowComponent, inServiceProvider)
        {
        }

        /* -------- Public Methods -------- */
        public override void Update(GameTimeWrapper gtw)
        {
            // DEBUG
            flowComponent.SubmitFlowStateChangeRequest(new FlowStateChangeRequest(eTargetFlowState.GAME));
        }

        public override void Draw(GraphicsComponent g)
        {
            // DEBUG
            g.Clear(Color.Red);
        }
    }
}
