﻿using System;
using Microsoft.Xna.Framework;

namespace GunGame
{
    class GameFlowState : FlowState
    {
        /* -------- Constructors -------- */
        public GameFlowState(LoadingScreen inLoadingScreen, FlowComponent inFlowComponent, IServiceProvider inServiceProvider)
            : base(inFlowComponent, inServiceProvider)
        {
            EnqueueLoadingProcesses(inLoadingScreen);
        }

        /* -------- Public Methods -------- */
        public override void Update(GameTimeWrapper gtw)
        {

        }

        public override void Draw(GraphicsComponent g)
        {
            // DEBUG
            g.Clear(Color.White);
        }

        /* -------- Private Methods -------- */
        private void EnqueueLoadingProcesses(LoadingScreen inLoadingScreen)
        {
            // TODO
        }
    }
}
