﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace GunGame
{
    static class AssetLists
    {
        /* -------- Private Fields -------- */
        private static Dictionary<string, string> textureAssetDict;

        /* -------- Public Methods -------- */
        public static void LoadContent(ContentManager content)
        {
            AssetListItem[] textureAssetList = content.Load<AssetListItem[]>("XML/Asset Lists/TextureAssetList");
            textureAssetDict = new Dictionary<string, string>(textureAssetList.Length);
            foreach (AssetListItem ali in textureAssetList)
                if (!textureAssetDict.ContainsKey(ali.id))
                    textureAssetDict.Add(ali.id, ali.contentPath);
        }

        public static string GetTextureContentPath(string id)
        {
            if (!textureAssetDict.ContainsKey(id))
                return null;

            return textureAssetDict[id];
        }
    }
}
