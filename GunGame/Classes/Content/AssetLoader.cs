﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace GunGame
{
    static class AssetLoader
    {
        /* -------- Private Fields -------- */
        private static Texture2D defaultTexture;

        /* -------- Public Methods -------- */
        public static void LoadContent(ContentManager content)
        {
            defaultTexture = content.Load<Texture2D>("Defaults/DefaultTexture");
        }

        public static Texture2D LoadTexture(string id, ContentManager content)
        {
            string contentPath = AssetLists.GetTextureContentPath(id);

            if (string.IsNullOrEmpty(contentPath))
                return defaultTexture;

            return content.Load<Texture2D>(contentPath);
        }
    }
}
