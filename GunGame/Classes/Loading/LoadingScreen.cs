﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace GunGame
{
    class LoadingScreen
    {
        /* -------- Properties -------- */
        public Queue<LoadingProcess> LoadingProcessQueue { get; private set; }
        public bool HasLoadingToProcess => LoadingProcessQueue.Count > 0;

        /* -------- Constructors -------- */
        public LoadingScreen()
        {
            LoadingProcessQueue = new Queue<LoadingProcess>();
        }

        /* -------- Public Methods -------- */
        public void Update()
        {
            LoadingProcess process = LoadingProcessQueue.Dequeue();
            process.Process();
        }

        public void Draw(GraphicsComponent g)
        {
            // DEBUG
            g.Clear(Color.Blue);
        }
    }
}
