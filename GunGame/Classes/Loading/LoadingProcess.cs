﻿namespace GunGame
{
    abstract class LoadingProcess
    {
        /* -------- Public Methods -------- */
        public abstract void Process();
    }
}
