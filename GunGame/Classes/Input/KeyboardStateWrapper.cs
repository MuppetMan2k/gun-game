﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace GunGame
{
    class KeyboardStateWrapper
    {
        /* -------- Private Fields -------- */
        private Dictionary<Keys, eButtonState> buttonStateDict;

        /* -------- Constructors -------- */
        public KeyboardStateWrapper()
        {
            buttonStateDict = new Dictionary<Keys, eButtonState>();
            foreach (Keys k in EnumUtils.GetEnumStates<Keys>())
                buttonStateDict.Add(k, eButtonState.RELEASED);
        }

        /* -------- Public Methods -------- */
        public void Update(KeyboardState state)
        {
            foreach (Keys k in EnumUtils.GetEnumStates<Keys>())
            {
                if (state.IsKeyDown(k))
                {
                    switch (GetButtonState(k))
                    {
                        case eButtonState.RELEASED:
                            SetButtonState(k, eButtonState.PRESSING);
                            break;
                        case eButtonState.PRESSING:
                            SetButtonState(k, eButtonState.PRESSED);
                            break;
                        case eButtonState.PRESSED:
                            break;
                        case eButtonState.RELEASING:
                            SetButtonState(k, eButtonState.PRESSING);
                            break;
                    }
                }
                else
                {
                    switch (GetButtonState(k))
                    {
                        case eButtonState.RELEASED:
                            break;
                        case eButtonState.PRESSING:
                            SetButtonState(k, eButtonState.RELEASING);
                            break;
                        case eButtonState.PRESSED:
                            SetButtonState(k, eButtonState.RELEASING);
                            break;
                        case eButtonState.RELEASING:
                            SetButtonState(k, eButtonState.RELEASED);
                            break;
                    }
                }
            }
        }

        public eButtonState GetButtonState(Keys key) => buttonStateDict[key];
        public bool ButtonIsPressing(Keys key) => GetButtonState(key) == eButtonState.PRESSING;
        public bool ButtonIsPressingOrPressed(Keys key) => GetButtonState(key) == eButtonState.PRESSING || GetButtonState(key) == eButtonState.PRESSED;

        /* -------- Private Methods -------- */
        private void SetButtonState(Keys key, eButtonState state)
        {
            buttonStateDict[key] = state;
        }
    }
}
