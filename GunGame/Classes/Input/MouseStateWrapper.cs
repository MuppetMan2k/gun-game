﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GunGame
{
    /* -------- Enumerations -------- */
    enum eMouseButton
    {
        LEFT,
        MIDDLE,
        RIGHT,
        X1,
        X2
    }

    class MouseStateWrapper
    {
        /* -------- Properties -------- */
        public Point PSPosition { get; private set; }
        public int RelativeScrollValue { get; private set; }

        /* -------- Private Fields -------- */
        private Dictionary<eMouseButton, eButtonState> buttonStateDict;
        private int prevScrollValue = 0;

        /* -------- Constructors -------- */
        public MouseStateWrapper()
        {
            buttonStateDict = new Dictionary<eMouseButton, eButtonState>();
            foreach (eMouseButton m in EnumUtils.GetEnumStates<eMouseButton>())
                buttonStateDict.Add(m, eButtonState.RELEASED);
        }

        /* -------- Public Methods -------- */
        public void Update(MouseState state)
        {
            #region LEFT
            switch (GetButtonState(eMouseButton.LEFT))
            {
                case eButtonState.RELEASED:
                    if (state.LeftButton == ButtonState.Pressed)
                        SetButtonState(eMouseButton.LEFT, eButtonState.PRESSING);
                    break;
                case eButtonState.PRESSING:
                    if (state.LeftButton == ButtonState.Pressed)
                        SetButtonState(eMouseButton.LEFT, eButtonState.PRESSED);
                    else
                        SetButtonState(eMouseButton.LEFT, eButtonState.RELEASING);
                    break;
                case eButtonState.PRESSED:
                    if (state.LeftButton == ButtonState.Released)
                        SetButtonState(eMouseButton.LEFT, eButtonState.RELEASING);
                    break;
                case eButtonState.RELEASING:
                    if (state.LeftButton == ButtonState.Pressed)
                        SetButtonState(eMouseButton.LEFT, eButtonState.PRESSING);
                    else
                        SetButtonState(eMouseButton.LEFT, eButtonState.RELEASED);
                    break;
            }
            #endregion

            #region MIDDLE
            switch (GetButtonState(eMouseButton.MIDDLE))
            {
                case eButtonState.RELEASED:
                    if (state.MiddleButton == ButtonState.Pressed)
                        SetButtonState(eMouseButton.MIDDLE, eButtonState.PRESSING);
                    break;
                case eButtonState.PRESSING:
                    if (state.MiddleButton == ButtonState.Pressed)
                        SetButtonState(eMouseButton.MIDDLE, eButtonState.PRESSED);
                    else
                        SetButtonState(eMouseButton.MIDDLE, eButtonState.RELEASING);
                    break;
                case eButtonState.PRESSED:
                    if (state.MiddleButton == ButtonState.Released)
                        SetButtonState(eMouseButton.MIDDLE, eButtonState.RELEASING);
                    break;
                case eButtonState.RELEASING:
                    if (state.MiddleButton == ButtonState.Pressed)
                        SetButtonState(eMouseButton.MIDDLE, eButtonState.PRESSING);
                    else
                        SetButtonState(eMouseButton.MIDDLE, eButtonState.RELEASED);
                    break;
            }
            #endregion

            #region RIGHT
            switch (GetButtonState(eMouseButton.RIGHT))
            {
                case eButtonState.RELEASED:
                    if (state.RightButton == ButtonState.Pressed)
                        SetButtonState(eMouseButton.RIGHT, eButtonState.PRESSING);
                    break;
                case eButtonState.PRESSING:
                    if (state.RightButton == ButtonState.Pressed)
                        SetButtonState(eMouseButton.RIGHT, eButtonState.PRESSED);
                    else
                        SetButtonState(eMouseButton.RIGHT, eButtonState.RELEASING);
                    break;
                case eButtonState.PRESSED:
                    if (state.RightButton == ButtonState.Released)
                        SetButtonState(eMouseButton.RIGHT, eButtonState.RELEASING);
                    break;
                case eButtonState.RELEASING:
                    if (state.RightButton == ButtonState.Pressed)
                        SetButtonState(eMouseButton.RIGHT, eButtonState.PRESSING);
                    else
                        SetButtonState(eMouseButton.RIGHT, eButtonState.RELEASED);
                    break;
            }
            #endregion

            #region X1
            switch (GetButtonState(eMouseButton.X1))
            {
                case eButtonState.RELEASED:
                    if (state.XButton1 == ButtonState.Pressed)
                        SetButtonState(eMouseButton.X1, eButtonState.PRESSING);
                    break;
                case eButtonState.PRESSING:
                    if (state.XButton1 == ButtonState.Pressed)
                        SetButtonState(eMouseButton.X1, eButtonState.PRESSED);
                    else
                        SetButtonState(eMouseButton.X1, eButtonState.RELEASING);
                    break;
                case eButtonState.PRESSED:
                    if (state.XButton1 == ButtonState.Released)
                        SetButtonState(eMouseButton.X1, eButtonState.RELEASING);
                    break;
                case eButtonState.RELEASING:
                    if (state.XButton1 == ButtonState.Pressed)
                        SetButtonState(eMouseButton.X1, eButtonState.PRESSING);
                    else
                        SetButtonState(eMouseButton.X1, eButtonState.RELEASED);
                    break;
            }
            #endregion

            #region X2
            switch (GetButtonState(eMouseButton.X2))
            {
                case eButtonState.RELEASED:
                    if (state.XButton2 == ButtonState.Pressed)
                        SetButtonState(eMouseButton.X2, eButtonState.PRESSING);
                    break;
                case eButtonState.PRESSING:
                    if (state.XButton2 == ButtonState.Pressed)
                        SetButtonState(eMouseButton.X2, eButtonState.PRESSED);
                    else
                        SetButtonState(eMouseButton.X2, eButtonState.RELEASING);
                    break;
                case eButtonState.PRESSED:
                    if (state.XButton2 == ButtonState.Released)
                        SetButtonState(eMouseButton.X2, eButtonState.RELEASING);
                    break;
                case eButtonState.RELEASING:
                    if (state.XButton2 == ButtonState.Pressed)
                        SetButtonState(eMouseButton.X2, eButtonState.PRESSING);
                    else
                        SetButtonState(eMouseButton.X2, eButtonState.RELEASED);
                    break;
            }
            #endregion

            PSPosition = state.Position;

            RelativeScrollValue = state.ScrollWheelValue - prevScrollValue;
            prevScrollValue = state.ScrollWheelValue;
        }

        public eButtonState GetButtonState(eMouseButton button) => buttonStateDict[button];
        public bool ButtonIsPressing(eMouseButton button) => GetButtonState(button) == eButtonState.PRESSING;
        public bool ButtonIsPressingOrPressed(eMouseButton button) => GetButtonState(button) == eButtonState.PRESSING || GetButtonState(button) == eButtonState.PRESSED;

        public Vector2 GetSSPosition(GraphicsComponent g) => SpaceUtils.ConvertPSToSS(PSPosition, g);

        /* -------- Private Methods -------- */
        private void SetButtonState(eMouseButton button, eButtonState state)
        {
            buttonStateDict[button] = state;
        }
    }
}
