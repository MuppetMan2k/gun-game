﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GunGame
{
    class GamePadStateWrapper
    {
        /* -------- Properties -------- */
        public Vector2 LeftStick { get; private set; }
        public Vector2 RightStick { get; private set; }
        public float LeftTrigger { get; private set; }
        public float RightTrigger { get; private set; }

        /* -------- Private Fields -------- */
        private Dictionary<Buttons, eButtonState> buttonStateDict;
        // Constants
        const float STICK_LOWER_DEAD_ZONE = 0.1f;
        const float STICK_UPPER_DEAD_ZONE = 0.9f;

        /* -------- Constructors -------- */
        public GamePadStateWrapper()
        {
            buttonStateDict = new Dictionary<Buttons, eButtonState>();
            foreach (Buttons b in EnumUtils.GetEnumStates<Buttons>())
                buttonStateDict.Add(b, eButtonState.RELEASED);
        }

        /* -------- Public Methods -------- */
        public void Update(GamePadState state)
        {
            foreach (Buttons b in EnumUtils.GetEnumStates<Buttons>())
            {
                switch (GetButtonState(b))
                {
                    case eButtonState.RELEASED:
                        if (state.IsButtonDown(b))
                            SetButtonState(b, eButtonState.PRESSING);
                        break;
                    case eButtonState.PRESSING:
                        if (state.IsButtonDown(b))
                            SetButtonState(b, eButtonState.PRESSED);
                        else
                            SetButtonState(b, eButtonState.RELEASING);
                        break;
                    case eButtonState.PRESSED:
                        if (state.IsButtonUp(b))
                            SetButtonState(b, eButtonState.RELEASING);
                        break;
                    case eButtonState.RELEASING:
                        if (state.IsButtonDown(b))
                            SetButtonState(b, eButtonState.PRESSING);
                        else
                            SetButtonState(b, eButtonState.RELEASED);
                        break;
                }
            }

            LeftStick = FilterStick(state.ThumbSticks.Left);
            RightStick = FilterStick(state.ThumbSticks.Right);

            LeftTrigger = state.Triggers.Left;
            RightTrigger = state.Triggers.Right;
        }

        public eButtonState GetButtonState(Buttons button) => buttonStateDict[button];
        public bool ButtonIsPressing(Buttons button) => GetButtonState(button) == eButtonState.PRESSING;
        public bool ButtonIsPressingOrPressed(Buttons button) => GetButtonState(button) == eButtonState.PRESSING || GetButtonState(button) == eButtonState.PRESSED;

        /* -------- Private Methods -------- */
        private void SetButtonState(Buttons button, eButtonState state)
        {
            buttonStateDict[button] = state;
        }

        private Vector2 FilterStick(Vector2 raw)
        {
            Vector2 filtered = raw;
            float mag = filtered.Length();

            if (mag < STICK_LOWER_DEAD_ZONE)
                filtered = Vector2.Zero;

            if (mag > STICK_UPPER_DEAD_ZONE)
                filtered = Vector2.Normalize(filtered);

            return filtered;
        }
    }
}
