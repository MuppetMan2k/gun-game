﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GunGame
{
    /* -------- Enumerations -------- */
    enum eButtonState
    {
        RELEASED,
        PRESSING,
        PRESSED,
        RELEASING
    }

    class InputComponent
    {
        /* -------- Properties -------- */
        public KeyboardStateWrapper KeyboardStateWrapper { get; private set; }
        public MouseStateWrapper MouseStateWrapper { get; private set; }
        public GamePadStateWrapper[] GamePadStateWrappers { get; private set; }
        public List<GameController> GameControllers { get; private set; }
        public GameController PrimaryGameController => GameControllers.Count > 0 ? GameControllers[0] : null;

        /* -------- Constructors -------- */
        public InputComponent()
        {
            KeyboardStateWrapper = new KeyboardStateWrapper();
            MouseStateWrapper = new MouseStateWrapper();
            GamePadStateWrappers = new GamePadStateWrapper[4];
            for (int i = 0; i < 4; i++)
                GamePadStateWrappers[i] = new GamePadStateWrapper();

            GameControllers = new List<GameController>();

            // DEBUG
            GameControllers.Add(new GameController(GamePadStateWrappers[0]));
        }

        /* -------- Public Methods -------- */
        public void Update()
        {
            KeyboardStateWrapper.Update(Keyboard.GetState());
            MouseStateWrapper.Update(Mouse.GetState());
            GamePadStateWrappers[0].Update(GamePad.GetState(PlayerIndex.One, GamePadDeadZone.None));
            GamePadStateWrappers[1].Update(GamePad.GetState(PlayerIndex.Two, GamePadDeadZone.None));
            GamePadStateWrappers[2].Update(GamePad.GetState(PlayerIndex.Three, GamePadDeadZone.None));
            GamePadStateWrappers[3].Update(GamePad.GetState(PlayerIndex.Four, GamePadDeadZone.None));
        }
    }
}
