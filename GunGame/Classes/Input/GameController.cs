﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GunGame
{
    /* -------- Enumerations -------- */
    public enum eControllerInputType
    {
        KEYBOARD_AND_MOUSE,
        GAME_PAD
    }

    class GameController
    {
        /* -------- Properties -------- */
        public eControllerInputType Type { get; private set; }
        // Controls
        public Vector2 MoveControl { get; private set; }
        public Vector2 ViewControl { get; private set; }
        public float RightControl { get; private set; }
        public float LeftControl { get; private set; }
        public bool ReloadControl { get; private set; }
        public bool CockControl { get; private set; }
        public bool MenuUpControl { get; private set; }
        public bool MenuDownControl { get; private set; }
        public bool MenuLeftControl { get; private set; }
        public bool MenuRightControl { get; private set; }
        public bool MenuAcceptControl { get; private set; }
        public bool MenuBackControl { get; private set; }
        public bool ExitControl { get; private set; }

        /* -------- Private Fields -------- */
        private KeyboardStateWrapper ksw;
        private MouseStateWrapper msw;
        private GamePadStateWrapper gpsw;
        // Constants
        private const float MOUSE_SS_VERTICAL_RADIUS = 0.1f;
        private const Keys KEYBOARD_UP = Keys.W;
        private const Keys KEYBOARD_LEFT = Keys.A;
        private const Keys KEYBOARD_DOWN = Keys.S;
        private const Keys KEYBOARD_RIGHT = Keys.D;
        private const Keys KEYBOARD_RELOAD = Keys.R;
        private const Keys KEYBOARD_COCK = Keys.E;
        private const Keys KEYBOARD_MENU_UP = Keys.Up;
        private const Keys KEYBOARD_MENU_DOWN = Keys.Down;
        private const Keys KEYBOARD_MENU_LEFT = Keys.Left;
        private const Keys KEYBOARD_MENU_RIGHT = Keys.Right;
        private const Keys KEYBOARD_MENU_ACCEPT = Keys.Enter;
        private const Keys KEYBOARD_MENU_BACK = Keys.Back;
        private const Keys KEYBOARD_EXIT = Keys.Escape;
        private const eMouseButton MOUSE_RIGHT = eMouseButton.LEFT;
        private const eMouseButton MOUSE_LEFT = eMouseButton.RIGHT;
        private const Buttons GAME_PAD_RELOAD = Buttons.X;
        private const Buttons GAME_PAD_COCK = Buttons.B;
        private const Buttons GAME_PAD_MENU_UP = Buttons.LeftThumbstickUp;
        private const Buttons GAME_PAD_MENU_DOWN = Buttons.LeftThumbstickDown;
        private const Buttons GAME_PAD_MENU_LEFT = Buttons.LeftThumbstickLeft;
        private const Buttons GAME_PAD_MENU_RIGHT = Buttons.LeftThumbstickRight;
        private const Buttons GAME_PAD_MENU_ACCEPT = Buttons.A;
        private const Buttons GAME_PAD_MENU_BACK = Buttons.B;
        private const Buttons GAME_PAD_EXIT = Buttons.Start;

        /* -------- Constructors -------- */
        public GameController(KeyboardStateWrapper inKSW, MouseStateWrapper inMSW)
        {
            Type = eControllerInputType.KEYBOARD_AND_MOUSE;
            ksw = inKSW;
            msw = inMSW;
        }
        public GameController(GamePadStateWrapper inGPSW)
        {
            Type = eControllerInputType.GAME_PAD;
            gpsw = inGPSW;
        }

        /* -------- Public Methods -------- */
        public void Update(GraphicsComponent g)
        {
            switch (Type)
            {
                case eControllerInputType.KEYBOARD_AND_MOUSE:
                    Update_KeyboardAndMouse(g);
                    break;
                case eControllerInputType.GAME_PAD:
                    Update_GamePad();
                    break;
            }
        }

        /* -------- Private Methods -------- */
        private void Update_KeyboardAndMouse(GraphicsComponent g)
        {
            Vector2 newMoveControl = Vector2.Zero;
            if (ksw.ButtonIsPressingOrPressed(KEYBOARD_UP))
                newMoveControl.Y += 1f;
            if (ksw.ButtonIsPressingOrPressed(KEYBOARD_RIGHT))
                newMoveControl.X -= 1f;
            if (ksw.ButtonIsPressingOrPressed(KEYBOARD_DOWN))
                newMoveControl.Y -= 1f;
            if (ksw.ButtonIsPressingOrPressed(KEYBOARD_RIGHT))
                newMoveControl.X += 1f;
            if (newMoveControl != Vector2.Zero)
                newMoveControl = Vector2.Normalize(newMoveControl);
            MoveControl = newMoveControl;

            Vector2 newViewControl = msw.GetSSPosition(g);
            newViewControl.X /= (MOUSE_SS_VERTICAL_RADIUS / g.AspectRatio);
            newViewControl.Y /= MOUSE_SS_VERTICAL_RADIUS;
            if (newViewControl != Vector2.Zero)
                newViewControl = Vector2.Normalize(newViewControl);
            ViewControl = newViewControl;

            RightControl = msw.ButtonIsPressingOrPressed(MOUSE_RIGHT) ? 1f : 0f;
            LeftControl = msw.ButtonIsPressingOrPressed(MOUSE_LEFT) ? 1f : 0f;
            ReloadControl = ksw.ButtonIsPressing(KEYBOARD_RELOAD);
            CockControl = ksw.ButtonIsPressing(KEYBOARD_COCK);

            MenuUpControl = ksw.ButtonIsPressing(KEYBOARD_MENU_UP);
            MenuDownControl = ksw.ButtonIsPressing(KEYBOARD_MENU_DOWN);
            MenuLeftControl = ksw.ButtonIsPressing(KEYBOARD_MENU_LEFT);
            MenuRightControl = ksw.ButtonIsPressing(KEYBOARD_MENU_RIGHT);
            MenuAcceptControl = ksw.ButtonIsPressing(KEYBOARD_MENU_ACCEPT);
            MenuBackControl = ksw.ButtonIsPressing(KEYBOARD_MENU_BACK);
            ExitControl = ksw.ButtonIsPressing(KEYBOARD_EXIT);
        }
        private void Update_GamePad()
        {
            MoveControl = gpsw.LeftStick;
            ViewControl = gpsw.RightStick;
            RightControl = gpsw.RightTrigger;
            LeftControl = gpsw.LeftTrigger;
            ReloadControl = gpsw.ButtonIsPressing(GAME_PAD_RELOAD);
            CockControl = gpsw.ButtonIsPressing(GAME_PAD_COCK);

            MenuUpControl = gpsw.ButtonIsPressing(GAME_PAD_MENU_UP);
            MenuDownControl = gpsw.ButtonIsPressing(GAME_PAD_MENU_DOWN);
            MenuLeftControl = gpsw.ButtonIsPressing(GAME_PAD_MENU_LEFT);
            MenuRightControl = gpsw.ButtonIsPressing(GAME_PAD_MENU_RIGHT);
            MenuAcceptControl = gpsw.ButtonIsPressing(GAME_PAD_MENU_ACCEPT);
            MenuBackControl = gpsw.ButtonIsPressing(GAME_PAD_MENU_BACK);
            ExitControl = gpsw.ButtonIsPressing(GAME_PAD_EXIT);
        }
    }
}
