﻿using System;

namespace GunGame
{
#if WINDOWS || LINUX
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (GunGame game = new GunGame())
            {
#if DEBUG
                game.Run();
#else
                try
                {
                    game.Run();
                }
                catch (Exception e)
                {
                    game.Exit();

                    string errorMessage = string.Format("Encountered an uncought exception: {0}", e.Message);
                    MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
#endif
            }
        }
    }
#endif
}
