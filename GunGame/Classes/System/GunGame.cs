﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GunGame
{
    public class GunGame : Game
    {
        /* -------- Private Fields -------- */
        // Components
        GraphicsComponent graphics;
        InputComponent input;
        FlowComponent flow;

        /* -------- Constructors -------- */
        public GunGame()
        {
            graphics = new GraphicsComponent(this);
            input = new InputComponent();
            flow = new FlowComponent(Services);

            Content.RootDirectory = "Content";
        }

        /* -------- Private Methods -------- */
        protected override void Initialize()
        {
            graphics.SetGraphicsDevice(GraphicsDevice);

            // TODO: Add your initialization logic here

            base.Initialize();
        }
        
        protected override void LoadContent()
        {
            graphics.CreateSpriteBatch();

            // TODO: use this.Content to load your game content here
        }
        
        protected override void Update(GameTime gameTime)
        {
            GameTimeWrapper gtw = new GameTimeWrapper(gameTime);

            input.Update();

#if DEBUG
            if (input.KeyboardStateWrapper.ButtonIsPressing(Keys.Escape))
            {
                Exit();
            }
#endif

            flow.Update(gtw);

            base.Update(gameTime);
        }
        
        protected override void Draw(GameTime gameTime)
        {
            graphics.Clear(Color.Black);
            flow.Draw(graphics);

            base.Draw(gameTime);
        }
    }
}
